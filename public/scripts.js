/* jshint browser: true */
/*
(function () {
"use strict";
window.onload = init;
function init () {
	//DOM elements
	var nav = document.querySelector('.nav');

	var headroom  = new Headroom(nav);

	window.onscroll = function (e) {
		if(document.body.scrollTop !== 0) {
			console.log('not top');
			addClass(nav, 'scrolled');
		}	
		if(document.body.scrollTop === 0) {
			console.log('top');
			removeClass(nav, 'scrolled');
		}
	};
}
function addClass(el, className) {
  if (el.classList)
    el.classList.add(className);
  else if (!hasClass(el, className))
		el.className += " " + className;
}
function removeClass(el, className) {
  if (el.classList)
    el.classList.remove(className);
  else if (hasClass(el, className)) {
    var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
    el.className=el.className.replace(reg, ' ');
  }
}
})();
*/
(function () {
"use strict";

var myElement = document.querySelector("header");
var headroom  = new Headroom(myElement);
headroom.init(); 
})();