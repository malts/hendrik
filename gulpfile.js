var gulp 		= require('gulp'),
		pug			=	require('gulp-pug'),
		sass		= require('gulp-sass'),
		jshint	= require('gulp-jshint'),
		stylish	= require('jshint-stylish'),
		server	=	require('gulp-webserver');

gulp.task('html', ['css'], function(){
	return gulp.src('source/*.pug')
	.pipe(pug())
	.on('error', swallowError)
	.pipe(gulp.dest('public'))
});
gulp.task('css', function(){
	return gulp.src('source/**/*.sass')
	.pipe(sass({outputStyle: 'compressed'}))
	.on('error', swallowError)
	.pipe(gulp.dest('public'))
});
gulp.task('media', function () {
	return gulp.src('source/media/*.*')
	.pipe(gulp.dest('public/media'))
});
gulp.task('js', function () {
	return gulp.src('source/**/*.js')
	.pipe(jshint())
	.pipe(jshint.reporter(stylish))
	.pipe(gulp.dest('public'))
});
gulp.task('webserver', function () {
	gulp.src('./')
	.pipe(server({
		livereload: true,
		directoryListing: true,
		open: true
	}))
});
gulp.task('watch', function(){
	gulp.watch('source/**/*.pug', ['html']);
	gulp.watch('source/**/*.sass', ['css']);
	gulp.watch('source/**/*.js', ['js']);
	gulp.watch('source/media/*.*', ['media'])
});

gulp.task('default', ['webserver', 'watch']);
gulp.task('build', ['html', 'css', 'js', 'media']);
function swallowError (error) {
  console.log(error.toString());
  this.emit('end');
}