# Henrdik kuhle Website
Gemacht mit Ɛ> von Malte Janßen
## Aufbau der Website
```
├── public //die kompilierten website-Dateien
├── source //die Rohdateien, geschrieben in Sass und Pug
    └── index.pug //die Index Datei
├── gulpfile.js //die Funktionen zum kompilieren der Website
├── package.json //alle Info's die zum installieren der Tools benötigt werden
└── README.md //diese Datei
```